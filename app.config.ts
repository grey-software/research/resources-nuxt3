export default defineAppConfig({
  docus: {
    title: "Grey Software Resources",
    description: "A collection of resources to help students of computer science, software development, and open source.",
    url: "https://resources.grey.software",
    image: "cover.png",
    socials: {
      twitter: "@grey_software",
      github: "grey-software",
      instagram: "greysoftware",
    },
    layout: "default",
    aside: {
      level: 1,
      filter: [],
    },
    header: {
      title: false,
      logo: true,
      showLinkIcon: false,
    },
    footer: {
      credits: {
        icon: "IconDocus",
        text: "Powered by Docus",
        href: "https://docus.dev",
      },
      textLinks: [
        {
          text: "Grey Software",
          href: "https://grey.software",
          target: "_blank",
          rel: "noopener",
        },
        {
          text: "Gitlab",
          href: "https://gitlab.com/grey-software",
          target: "_blank",
          rel: "noopener",
        },
        {
          text: "LinkedIn",
          href: "https://www.linkedin.com/company/grey-software",
          target: "_blank",
          rel: "noopener",
        },
      ],
    },
  },
});
