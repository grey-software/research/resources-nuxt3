const fs = require('fs');

function csvToMarkdown(csv) {
    // Split the CSV string into an array
    const parts = csv.split(',');

    // Extract each part of the CSV string and remove the single quotes around it 
    const title = parts[0] ? parts[0].replace(/'/g, '') : '';
    const description = parts[1] ? parts[1].replace(/'/g, '') : '';
    const link = parts[2] ? parts[2].replace(/'/g, '') : '';
    const tags = parts[3] ? parts[3].replace(/'/g, '') : '';

    console.log(title, description, link, tags)

    // Format the description, splitting it into two parts if it contains a period
    const formattedDescription = description

    // Format the tags, replacing each '|' character with a badge in markdown
    const formattedTags = tags.split('|')
        .map(tag => `:badge[${tag}]{type="info"}`)
        .join(' ');

    // Build the markdown string
    const markdown = `  ::card 
  #title
  ${title}
  #description
  ${formattedDescription}

  <div style="font-size:24px;margin:0;padding:0;">

  [Explore Cheat Sheet](${link})
  </div>
  <div style="display:flex;flex-direction:row-reverse">

  ${formattedTags}
  </div>
  ::`;

    // Return the markdown string
    return markdown;
}

// open resources.csv, and convert each line after the first to markdown. Output the markdown to resources.md
const csv = fs.readFileSync('resources.csv', 'utf8');
const lines = csv.split('\n');
for (let i = 1; i < lines.length; i++) {
    lines[i] = csvToMarkdown(lines[i]);
}
const markdown = lines.slice(1).join('\n');
fs.writeFileSync('resources.md', markdown);

