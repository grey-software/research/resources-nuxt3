  ::card 
  #title
  JavaScript Cheatsheet
  #description
  A comprehensive JavaScript cheatsheet that covers all the basics and more.

  <div style="font-size:24px;margin:0;padding:0;">

  [Explore Cheat Sheet](https://dev.to/samanthaming/a-complete-guide-to-javascript-tooling-1ogo)
  </div>
  <div style="display:flex;flex-direction:row-reverse">

  :badge[Free]{type="info"} :badge[Cheat Sheet
]{type="info"}
  </div>
  ::
  ::card 
  #title
  JavaScript YouTube Playlist
  #description
  A YouTube playlist by Traversy Media that covers JavaScript fundamentals.

  <div style="font-size:24px;margin:0;padding:0;">

  [Start Learning](https://dev.to/rose/javascript-youtube-playlist-1kbl)
  </div>
  <div style="display:flex;flex-direction:row-reverse">

  :badge[Free]{type="info"} :badge[YouTube Playlist
]{type="info"}
  </div>
  ::
  ::card 
  #title
  Scrimba JavaScript Course
  #description
  An interactive course on Scrimba that teaches JavaScript for beginners.

  <div style="font-size:24px;margin:0;padding:0;">

  [Start Learning](https://dev.to/rose/learn-javascript-with-scrimba-4non)
  </div>
  <div style="display:flex;flex-direction:row-reverse">

  :badge[Free]{type="info"} :badge[Scrimba Course
]{type="info"}
  </div>
  ::
  ::card 
  #title
  JavaScript Game
  #description
  A fun game on CodinGame that helps you learn JavaScript.

  <div style="font-size:24px;margin:0;padding:0;">

  [Play Now](https://dev.to/thelegendski/codingame-clash-of-code-hmb)
  </div>
  <div style="display:flex;flex-direction:row-reverse">

  :badge[Free]{type="info"} :badge[Game
]{type="info"}
  </div>
  ::
  ::card 
  #title
  
  #description
  

  <div style="font-size:24px;margin:0;padding:0;">

  [Explore Cheat Sheet]()
  </div>
  <div style="display:flex;flex-direction:row-reverse">

  :badge[]{type="info"}
  </div>
  ::